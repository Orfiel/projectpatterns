package orfiel.model;

public class PersonBuilder {


    String name, street;
    int homeAdres;
    Address address;

    public PersonBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PersonBuilder withStreet(String street) {
        this.street = street;
        return this;
    }

    public PersonBuilder withHomeAdres(int homeAdres) {
        this.homeAdres = homeAdres;
        return this;
    }

    public PersonBuilder withaddress(Address address) {
        this.address = address;
        return this;
    }


    public Person build() {
        Person p = new Person();
        p.setName(this.name);
        p.setAddress(this.address);
        Address address = new Address();
        address.setStreet(this.street);
        address.setHomeNumber(this.homeAdres);
        return p;
    }


}
